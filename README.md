**ffbatch:** A simple shell script for batch encoding videos with ffmpeg using Nvidia's NVENC and then renaming them with Filebot. This script is set up for mkv files created by a program like MakeMKV and will name files based on detected source where possible.

**Dependencies**
*  ffmpeg compiled with support for NVENC/NVDEC/CUVID
*  Filebot CLI


**Usage:** 
1. Make the script executable

`chmod +x ffbatch`

2. Edit 'OUTPUTDIR' and 'FILEBOTDIR' in ffbatch' to the desired locations

`OUTPUTDIR="/path/to/temporary/directory`
`FILEBOT="/path/to/filebot/output`

2. Navigate to the location of your video files

`cd /path/to/your/videos`

3. Execute ffbatch with optional quality (quality is entered as an integer and matches the cq for NVENC), if no quality is entered ffbatch will default to a cq of 18

`/path/to/ffbatch <quality>`

4. Optionally update Filebot format expressions as desired